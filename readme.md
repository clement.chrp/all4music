**Fichier "etl.py" qui enregistre dans un json les albums de 3 artistes** <br> 
**Classe Album et Classe AlbumRepository:**  
* Renvoie les albums d'un artiste
* Renvoie tous les albums triés par date de sortie
* Ajouter un album
* Supprimer un album
* Modifier un album
* Sauvegarder -> on recharge la nouvelle liste quand on relance  <br> 
**Classe User et UserRepository:**  
* Emprunter un album (on ne peut pas emprunter un album déjà emprunté)
* Renvoie la liste des albums empruntés
* Renvoie la liste des albums les plus empruntés
