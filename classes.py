import json

def transform_Albumsjson_file_to_object(file):
    
    object_list = []

    json_data = json.load(open(file))
    
    for album in json_data: 
        title = album['title']
        artist = album['artist']
        year = album['year']
        loan = album['loan']
        loan_count = album['loan_count']
        album = Album(title, artist, year, loan, loan_count)
        object_list.append(album)
    
    return object_list 


def transform_Usersjson_file_to_object(file):
    
    object_list = []

    json_data = json.load(open(file))
    
    for user in json_data: 
        name = user['name']
        user = User(name)
        object_list.append(user)
    
    return object_list 



def update_JSONfile(new_data):
    
    list_JSON = []
    
    for i in new_data:  
        list_JSON.append(i.__dict__)
  
    with open('albums.json', 'w') as f:
        json.dump(list_JSON, f, indent=4)





class Album:
    
    title: str
    artist: str
    year: int
    loan: str
    loan_count: int
    
    # Constructeur
    def __init__(self, title, artist, year, loan, loan_count):
        self.title = title
        self.artist = artist
        self.year = year
        self.loan = loan
        self.loan_count = loan_count
    
    def __str__(self):
        return f"artist: {self.artist}, title: {self.title}, year: ({self.year}),loan: {self.loan}, loan_count: {self.loan_count}"
    
    def __repr__(self):
        return self.__str__()

    def __eq__(self, other: object) -> bool:
        return self.artist == other.artist and self.title == other.title and self.year == other.year and self.loan == other.loan and self.loan_count == other.loan_count


class User:
    
    name: str
    
    # Constructeur
    def __init__(self, name):
        self.name = name
    
    def __str__(self):
        return f"name is : {self.name}"

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other: object) -> bool:
        return self.name == other.name


class Library:
    
    albums_list: list[Album]
    users_list: list[User]
    
    # Constructeur
    def __init__(self, albums_list, users_list):
        self.albums_list = albums_list
        self.users_list = users_list


    def get_library_list(self, list):
        for i in self.__getattribute__(list):
                print(i)

        return self.__getattribute__(list)

    
    def get_albums_from_one_artist(self, artist):
    
        artist_albums_list = []
        for album in self.albums_list:
            if album.artist == artist:
                artist_albums_list.append(album)
        
        for i in artist_albums_list: 
            print(i)
        
        return artist_albums_list



    def albums_sort_by_date_and_artist(self):
        self.albums_list.sort(key=lambda album: (album.artist, album.year))

        for i in self.albums_list: 
            print(i)

    def albums_sort_by_loan_count(self):
        self.albums_list.sort(key=lambda album: (album.loan_count), reverse=True)

        for i in self.albums_list: 
            print(i)


    def find_album(self, title, artist, year, loan="", loan_count=0):
        album_to_find = Album(title, artist, year, loan, loan_count)
        for album in self.albums_list:
            if album == album_to_find:
                return album


    def add_album(self, title, artist, year, loan="", loan_count=0):
        album = Album(title, artist, year, loan, loan_count)
        self.albums_list.append(album)
        update_JSONfile(self.albums_list)

        for i in self.albums_list:
            print(i)

    def remove_album(self, album: Album):
        self.albums_list.remove(album)
        update_JSONfile(self.albums_list)

        for i in self.albums_list:
            print(i)
    

    def modify_album(self, album: Album, change, new_name):
        for album_in_list in self.albums_list:
            if album.__getattribute__(change) == album_in_list.__getattribute__(change):
                album.__setattr__(change, new_name)
                update_JSONfile(self.albums_list)
        
        for i in self.albums_list:
            print(i)


    def loan_album(self, loan_name: User, loan_album: Album):
        for album_in_list in self.albums_list:
            if loan_album == album_in_list and album_in_list.loan == "":
                album_in_list.loan_count =+ 1
                album_in_list.loan = loan_name
                update_JSONfile(self.albums_list)
            if album_in_list.loan != "":
                print("Il est pas dispo, retente ta chance")

        for i in self.albums_list:
            print(i)

    def albums_not_available(self):
        
        albums_not_available_list = []

        for album in self.albums_list:
            if album.loan != "":
                albums_not_available_list.append(album)
        
        for i in albums_not_available_list:
            print(i)


users_repository = transform_Usersjson_file_to_object("users.json")
albums_repository = transform_Albumsjson_file_to_object("albums.json")
library_repository = Library(albums_repository, users_repository)
# library_repository.get_library_list("users_list")


# Eminem = library_repository.get_albums_from_one_artist("Eminem")
# JJG = library_repository.get_albums_from_one_artist("Jean-Jacques Goldman")
# Jul = library_repository.get_albums_from_one_artist("Jul")


# library_repository.albums_sort_by_date_and_artist()
# library_repository.albums_sort_by_loan_count()

# library_repository.add_album("les zouzous", "Zebda", 1985)

# album_to_remove = library_repository.find_album("Music To Be Murdered By (Side B)", "Eminem", "2021")
# library_repository.remove_album(album_to_remove)

# album_to_change = library_repository.find_album("Music to Be Murdered By", "Eminem", "2020")
# library_repository.modify_album(album_to_change, 'artist', "M&MS")


# test = library_repository.albums_list[0]
# test2 = library_repository.albums_list[1]
# library_repository.loan_album('Jean-Clement', test)
# library_repository.loan_album('Jean-Cyril', test)
# library_repository.loan_album('Jean-Cyril', test2)
# library_repository.loan_album('Jean-Clement', test2)
# print(library_repository.albums_list[0])

library_repository.albums_not_available()


# print(library_repository.albums_list)
