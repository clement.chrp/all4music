from bs4 import BeautifulSoup
import requests 
import json

artists = ["eminem", "jul", "jean-jacques-goldman"]

def albums_A4M_JSON(artists):
    """create JSON file with """
    
    albums_list = []
    
    for artist in artists:
    
        url=f"https://www.allformusic.fr/{artist}/discographie"
        content_html = BeautifulSoup(((requests.get(url)).content), "html.parser")

        titre_articles = content_html.find(id="disco-album").find("ol")
        for li in titre_articles: 
            title = li.strong.get_text(strip=True)
            year = li.span.get_text(strip=True)[-4:]
            artist = (content_html.find(id="artistName").find("strong")).get_text(strip=True)
            loan = ""
            loan_count = 0
            albums_list.append({
                "title": title,
                "artist": artist,
                "year": year,
                "loan": loan,
                "loan_count": loan_count})
            with open('albums.json', 'w') as f:
                json.dump(albums_list, f, indent=4)


if __name__=="__main__":
    albums_A4M_JSON(artists)
