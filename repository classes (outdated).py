class AlbumRepository:
    
    albums_list : list
    
    # Constructeur
    def __init__(self, albums_list):
        self.albums_list = albums_list
        
    # def get_albums_list_str_format(self):
    #     # for i in self.albums_list:
    #     #         print(i)
        
    #     return self.albums_list

    def get_albums_from_one_artist(self, author):
    
        films_lent = []
        for album in self.albums_list:
            if album.author == author:
                films_lent.append(album)
        
        for i in films_lent: 
            print(i)
        
        return films_lent


    def sort_by_date_and_author(self):
        self.albums_list.sort(key=lambda album: (album.author, album.year))
        for i in self.albums_list: 
            print(i)


    def add_album(self, title, author, year):
        album = Album(title, author, year)
        self.albums_list.append(album)
        update_JSONfile(self.albums_list)
        for i in self.albums_list:
            print(i)

    def remove_album(self, album: Album):
        self.albums_list.remove(album)
        update_JSONfile(self.albums_list)

        for i in self.albums_list:
            print(i)
    
    def find_album(self, title, author, year):
        album_to_find = Album(title, author, year)
        for album in self.albums_list:
            if album == album_to_find:
                return album

    def modify_album(self, album: Album, change, new_name):
        for album_in_list in self.albums_list:
            if album.__getattribute__(change) == album_in_list.__getattribute__(change):
                album.__setattr__(change, new_name)
                update_JSONfile(self.albums_list)
                print(self.albums_list)
        
        for i in self.albums_list:
            print(i)


class UsersRepository:
    
    users_list : list
    
    # Constructeur
    def __init__(self, users_list):
        self.users_list = users_list

    def get_user_list_str_format(self):
        for i in self.users_list:
                print(i)
        
        return self.users_list